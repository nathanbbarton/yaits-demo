Requirements
------------

Backend
* Go

Frontend
* NodeJs
* React

Ways to run
------------

1. For validation of the backend API, you can simply use go `run main.go from` inside the /backend directory. This will start the api server locally and use and embedded postgres database
2. For a full environment docker-compose can be used. From root of the project execute `docker-compose up -d --build` can be used

Description
------------

Design considerations: Standard API design, frontend react app, backend go service, using postgres as the database. I used Go libraries gorm, and gin to implement the overall structure. Project structured to separate the different areas of the project. 

https://gorm.io/index.html
https://github.com/gin-gonic/gin

Future Additions
----------------
* Increased filter/query functionality
* frontend proper integration
* user entity stored in database
* authentication with user
* integration with secrets service for proper storage of database parameters (password)
