// This file contains the necessary functions to action REST calls on the /issues endpoint

package controllers

import (
	"fmt"
	"net/http"
	"strings"

	"backend/database"
	"backend/models"

	"github.com/gin-gonic/gin"
)

func isOperator(r rune) bool {
	return r == '>' || r == '<' || r == '='
}

//Handle query parameters
//Assumption query comes in form (key, [operator], value)
//Given the opportunity I would re-work this
//Assumptions during parsing, & = AND, > = greater than, < = less than
//Additional features could include OR operator
func constructQuery(s string) (string, []string) {

	s = strings.Replace(s, "%3C", "<", -1)
	s = strings.Replace(s, "%3E", ">", -1)

	var querySlice [][]string //Building an array of string arrays
	var row []string
	var values []string
	key := ""
	value := ""
	count := 0 // 0 = key building, 1 = value building
	query := ""

	for index, char := range s {
		if char == '&' { // & operator or last next row, reset values
			values = append(values, value) // add the value to the row
			row = append(row, "?")
			querySlice = append(querySlice, row) // add the row to the full query slice
			row = nil
			count = 0
			key = ""
			value = ""
			continue
		}
		if isOperator(char) {
			row = append(row, key)          // Key side done, add to slice
			row = append(row, string(char)) // add operator to slice
			count = 1                       // move to value building
		} else {
			if count == 0 {
				key += string(char)
			} else if count == 1 {
				value += string(char)
				if index == len([]rune(s))-1 { // last iteration
					values = append(values, value)
					row = append(row, "?")
					querySlice = append(querySlice, row)
				}
			}
		}
	}

	for index, element := range querySlice { //Iterate through all query parameters

		for _, value := range element {
			query += value + " "
		}

		if index != len(querySlice)-1 { // not last iteration
			query += "AND "
		}
	}

	return query, values
}

// This function corresponds to a REST GET call on endpoint /issues
// Requires a pointer to the gin Context
// Returns all issues
func FindAllIssues(c *gin.Context) {
	fmt.Println("GET request made")

	// Open and Defer the close of the database connection
	db := database.OpenConnection()
	defer database.CloseConnection(db)

	query := c.Request.URL.RawQuery
	var issues []models.Issue
	var values []string

	if query != "" {
		query, values = constructQuery(query)

		valuesInterface := make([]interface{}, len(values)) //Need to convert slice to interface for db.Where call
		for i := range values {
			valuesInterface[i] = values[i]
		}
		// Find all issues and bind to model issue
		if result := db.Where(query, valuesInterface...).Find(&issues); result.Error != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
			return
		}
	} else {
		// Find all issues and bind to model issue
		if result := db.Find(&issues); result.Error != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
			return
		}
	}

	fmt.Printf("Result of FindAll: %+v\n", issues)

	// Respond with the found issues
	c.JSON(http.StatusOK, gin.H{
		"response": issues,
	})

}

// This function corresponds to a REST GET call on endpoint /issues
// Requires a pointer to the gin Context
// Returns issue based on ID
func FindIssue(c *gin.Context) {
	fmt.Println("GET request made")

	// Open and Defer the close of the database connection
	db := database.OpenConnection()
	defer database.CloseConnection(db)

	issueID := c.Param("ID")

	var issue models.Issue

	// Find issue and bind to model issue
	if result := db.First(&issue, issueID); result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	fmt.Printf("Result of Find: %+v\n", issue)

	// Respond with the found issues
	c.JSON(http.StatusOK, gin.H{
		"response": issue,
	})

}

// This method corresponds to a REST POST call on endpoint /issues
// Requires a pointer to the gin Context
func CreateIssue(c *gin.Context) {
	fmt.Println("POST request made")

	// Open and Defer the close of the database connection
	db := database.OpenConnection()
	defer database.CloseConnection(db)

	var issue models.Issue

	// Attempt to bind incoming JSON to the Issue model
	if err := c.ShouldBindJSON(&issue); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	fmt.Printf("%+v\n", issue)

	// Attempt to create new Issue to database
	if result := db.Create(&issue); result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	fmt.Println("POST request successful, Issue created")

	// Respond with successful creationg of Issue
	c.JSON(http.StatusOK, gin.H{
		"message": "Issue created successfully",
	})

}

// This method corresponds to a REST PUT call on endpoint /issues
// Requires a pointer to the gin Context
func ReplaceIssue(c *gin.Context) {
	fmt.Println("PUT request made")

	// Open and Defer the close of the database connection
	db := database.OpenConnection()
	defer database.CloseConnection(db)

	var issueReq models.Issue
	var issueDb models.Issue

	// Attempt to bind incoming JSON to the Issue model
	if err := c.ShouldBindJSON(&issueReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	fmt.Printf("Request Issue: %+v\n", issueReq)

	// Attempt to find issue by ID
	if result := db.First(&issueDb, issueReq.ID); result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	fmt.Printf("Database Issue: %+v\n", issueDb)

	issueDb = issueReq

	//Apply full save of the requested changes
	if result := db.Save(&issueDb); result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "PUT request successful",
	})
}

// This method corresponds to a REST PATCH call on endpoint /issues
// Requires a pointer to the gin Context
func UpdateIssue(c *gin.Context) {
	fmt.Println("PATCH request made")

	// Open and Defer the close of the database connection
	db := database.OpenConnection()
	defer database.CloseConnection(db)

	var issueReq models.Issue
	var issueDb models.Issue

	// Attempt to bind incoming JSON to the Issue model
	if err := c.ShouldBindJSON(&issueReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	fmt.Printf("Request Issue: %+v\n", issueReq)

	// Attempt to find issue by ID
	if result := db.First(&issueDb, issueReq.ID); result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	fmt.Printf("Database Issue: %+v\n", issueDb)

	//Apply the requested changes for the provided fields
	if result := db.Model(&issueDb).Select("*").Updates(issueReq); result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	c.JSON(200, gin.H{
		"message": "PATCH request successful",
	})
}

// This method corresponds to a REST DELETE call on endpoint /issues
// Requires a pointer to the gin Context
func DeleteIssue(c *gin.Context) {
	fmt.Println("Delete request made")

	// Open and Defer the close of the database connection
	db := database.OpenConnection()
	defer database.CloseConnection(db)

	var issue models.Issue

	// Attempt to bind incoming JSON to the Issue model
	if err := c.ShouldBindJSON(&issue); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	fmt.Printf("%+v\n", issue)

	// Attempt to Delete Issue from database
	if result := db.Delete(&issue, issue.ID); result.Error != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": result.Error})
		return
	}

	fmt.Println("DELETE request successful, Issue deleted")

	//Respond with succesful deletion
	c.JSON(http.StatusOK, gin.H{
		"message": "DELETE performed successfully",
	})
}

//HEAD /issues
func MetadataIssue(c *gin.Context) {
	fmt.Println("HEAD request made")
	c.JSON(200, gin.H{
		"message": "not implemeneted yet",
	})
}

//OPTIONS /issues
func InfoIssues(c *gin.Context) {
	fmt.Println("OPTIONS request made")
	c.JSON(200, gin.H{
		"message": "not implemeneted yet",
	})
}
