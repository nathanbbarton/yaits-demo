package main

import (
	"os"

	"backend/database"
	"backend/routes"

	embeddedpostgres "github.com/fergusstrange/embedded-postgres"
	"github.com/gin-gonic/gin"
	_ "github.com/lib/pq"
)

func main() {

	var postgres *embeddedpostgres.EmbeddedPostgres

	if os.Getenv("DATABASE_TYPE") != "default" {
		postgres = database.InitEmbeddedDB()
		defer postgres.Stop() //Wait for main to close then shutdown embedded postgres, data will be blown away
	}

	//Check for database table, create if not exist
	database.CheckTable()

	router := gin.Default()

	routes.IssuesRoutes(router)

	router.Run() //Listen and serve on localhost:8080

}
