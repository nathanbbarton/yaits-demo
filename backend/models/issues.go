package models

import "gorm.io/gorm"

type Issue struct {
	gorm.Model
	Description string `json:"description"`
	Details     string `json:"details"`
	Priority    int    `json:"priority"`
	Status      string `json:"status"`
	Assignee    string `json:"assignee"`
}
