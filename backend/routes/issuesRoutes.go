package routes

import (
	"fmt"

	"backend/controllers"

	"github.com/gin-gonic/gin"
)

/**
* IssuesRoutes
* @input pointer to gin Engine
* Sets the controller functions for each of the RESTful functions
**/

func IssuesRoutes(route *gin.Engine) {

	fmt.Println("Defining '/issues' routes and controller functions")

	issues := route.Group("/issues")
	{
		issues.GET("/", controllers.FindAllIssues)
		issues.GET("/:ID", controllers.FindIssue)
		issues.POST("/", controllers.CreateIssue)
		issues.PATCH("/", controllers.UpdateIssue)
		issues.PUT("/", controllers.ReplaceIssue)
		issues.DELETE("/", controllers.DeleteIssue)
		issues.HEAD("/", controllers.MetadataIssue)
		issues.OPTIONS("/", controllers.InfoIssues)
	}
}
