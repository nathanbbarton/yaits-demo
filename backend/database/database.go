package database

import (
	"fmt"

	"backend/models"

	embeddedpostgres "github.com/fergusstrange/embedded-postgres"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const (
	dbname   = "yaitsdb"
	user     = "postgres"
	password = "P@ssw0rd123!"
	port     = 5432
)

var host = "postgres" //Host location of the postgres database. default 'postgres' connect through docker-compose

func InitEmbeddedDB() *embeddedpostgres.EmbeddedPostgres {
	//Check for environment variable to determine which database type to use
	// Use embedded postgres for quick testing
	host = "localhost" //'postgres' no longer valid as we are not running in default mode
	postgres := embeddedpostgres.NewDatabase(embeddedpostgres.DefaultConfig().
		Username(user).
		Password(password).
		Database(dbname))

	if err := postgres.Start(); err != nil {
		fmt.Println("Error starting embedded postgres instance")
		panic(err)
	}

	return postgres

}

func OpenConnection() *gorm.DB {

	//Format the postgres connection string
	psqlConfig := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)

	//Attempt to open conncetion to database
	db, err := gorm.Open(postgres.Open(psqlConfig), &gorm.Config{})

	if err != nil {
		fmt.Println("Open connection failed")
		panic(err)
	}

	fmt.Println("Successful Database connection")

	return db
}

func CloseConnection(db *gorm.DB) {
	pg, err := db.DB()
	if err != nil {
		fmt.Println("Something when wrong closing the DB connection")
	}
	pg.Close()
}

func CheckTable() {

	db := OpenConnection()

	issue := models.Issue{}

	//AutoMigrate the Issue schema to the database
	db.AutoMigrate(&issue)

	CloseConnection(db)

}
